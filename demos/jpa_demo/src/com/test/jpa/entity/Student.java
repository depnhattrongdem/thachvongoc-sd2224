package com.test.jpa.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author thachvon
 *
 */
@Entity
@Table(name = "student")
public class Student implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "name", length = 5000, nullable = false)
	private String name;

	@OneToMany(mappedBy = "student", cascade = { CascadeType.PERSIST }, orphanRemoval = true)
	private List<OpenedCourse> courses = new ArrayList<>();

	public Student() {
	}

	public Student(String studentName) {
		this.name = studentName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<OpenedCourse> getCourses() {
		return courses;
	}

	public void setCourses(List<OpenedCourse> courses) {
		this.courses = courses;
	}

	public void setStudentId(long studentId) {
		this.id = studentId;
	}

	public long getStudentId() {
		return id;
	}

	public void setStudentName(String studentName) {
		this.name = studentName;
	}

	public String getStudentName() {
		return name;
	}
}