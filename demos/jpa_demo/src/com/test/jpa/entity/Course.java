package com.test.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "course")
public class Course implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name", length = 100)
	private String name;

	@OneToMany(mappedBy = "course", cascade = { CascadeType.PERSIST }, orphanRemoval = true)
	private List<OpenedCourse> students = new ArrayList<>();

	public Course() {
	}

	public Course(String courseName) {
		this.name = courseName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<OpenedCourse> getStudents() {
		return students;
	}

	public void setStudents(List<OpenedCourse> students) {
		this.students = students;
	}

	public void addStudent(Student student, Date startDate, Date endDate) {
		OpenedCourse openedCourse = new OpenedCourse();
		openedCourse.setStartDate(startDate);
		openedCourse.setEndDate(endDate);
		openedCourse.setStudent(student);
		openedCourse.setCourse(this);
		this.students.add(openedCourse);
		student.getCourses().add(openedCourse);
	}
}
