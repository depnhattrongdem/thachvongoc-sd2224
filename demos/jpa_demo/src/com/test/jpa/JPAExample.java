package com.test.jpa;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;

import com.test.jpa.dao.EntityServiceImpl;
import com.test.jpa.dao.StudentService;
import com.test.jpa.entity.Course;
import com.test.jpa.entity.Student;

public class JPAExample {

	public static void main(String[] args) {
		EntityManager entityManager = EntityManagerUtil.getEntityManager();
		JPAExample.testInitData(entityManager);
		// JPAExample.testGetOpenedCourseByStudentId(entityManager, 1);
//		JPAExample.testUpdateStudent(entityManager);
//		JPAExample.testDeleteStudent(entityManager);
		JPAExample.testGetStudents(entityManager);
	}

	public static void testGetOpenedCourseByStudentId(EntityManager entityManager, long studentId) {
		StudentService studentService = new StudentService(entityManager);
		Student student = studentService.getOpenedCourseByStudentId(studentId);
		if (student != null)
			System.out.println("Student with name: " + student.getName() + " joined course with name: "
					+ student.getCourses().get(0).getCourse().getName());
		else
			System.out.println("No student with id: " + studentId);
	}

	public static void testGetStudents(EntityManager entityManager) {
		
		EntityServiceImpl<Student, Long> studentDAO = new EntityServiceImpl<>(entityManager, Student.class);
		List<Student> student = studentDAO.getAll();
		for (Student student2 : student) {
			System.out.println("Student with name: " + student2.getName());
		}		
	}

	public static void testDeleteStudent(EntityManager entityManager) {

		EntityServiceImpl<Student, Long> studentDAO = new EntityServiceImpl<>(entityManager, Student.class);
		List<Student> students = studentDAO.getAll();
		for (Student student : students) {
			studentDAO.delete(student);
		}
	}

	public static void testUpdateStudent(EntityManager entityManager) {
		
		EntityServiceImpl<Student, Long> studentDAO = new EntityServiceImpl<>(entityManager, Student.class);
		List<Student> students = studentDAO.getAll();
		for (Student student : students) {
			student.setName("Test2");
			studentDAO.save(student);
		}
	}

	public static void testInitData(EntityManager entityManager) {
		Date startDate = GregorianCalendar.getInstance().getTime();
		Date endDate = GregorianCalendar.getInstance().getTime();
		EntityServiceImpl<Course, Long> courseDAO = new EntityServiceImpl<>(entityManager, Course.class);

		System.out.println("Initialize some students");

		Student student1 = new Student("Thach");
		Student student2 = new Student("Thu");
		Student student3 = new Student("Duc");

		System.out.println("Initialize some courses");
		Course course1 = new Course("English");
		course1.addStudent(student1, startDate, endDate);
		course1.addStudent(student2, startDate, endDate);
		course1.addStudent(student3, startDate, endDate);
		courseDAO.save(course1);
	}
}
