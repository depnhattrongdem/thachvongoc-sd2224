package com.test.jpa.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.test.jpa.entity.Course;
import com.test.jpa.entity.OpenedCourse;
import com.test.jpa.entity.Student;

public class StudentService {
	EntityManager entityManager;

	public StudentService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void addToCourse(Student student, Course course, Date startDate, Date endDate){
		OpenedCourse openedCourse = new OpenedCourse();
		openedCourse.setStartDate(startDate);
		openedCourse.setEndDate(endDate);
		openedCourse.setStudent(student);
		openedCourse.setCourse(course);
		student.getCourses().add(openedCourse);		
	}	
	
	public void save(Student student) {		
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(student);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}

	public void update(Student student) {
		try {
			entityManager.getTransaction().begin();
			entityManager.find(Student.class, student.getId());			
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}
	
	public void delete(Student student) {
		try {
			entityManager.getTransaction().begin();			
			entityManager.remove(student);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}
	
	public List<Student> getAll(){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Student> cq = cb.createQuery(Student.class);
		
		Root<Student> from = cq.from(Student.class);
		CriteriaQuery<Student> select = cq.select(from);
		TypedQuery<Student> tq = entityManager.createQuery(select);
		List<Student> result = tq.getResultList();
		
		return result;
	}
	
	public Student getById(long studentId){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Student> cq = cb.createQuery(Student.class);		
		Root<Student> from = cq.from(Student.class);
		cq.select(from).where(cb.equal(from.get("id"),studentId));
		TypedQuery<Student> tq = entityManager.createQuery(cq);
		List<Student> result = tq.getResultList();
				
		return result.size() > 0 ? result.get(0) : null;
	}
	
	public Student getOpenedCourseByStudentId(long studentId){
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Student> cq = cb.createQuery(Student.class);
		Root<Student> fromStudent = cq.from(Student.class);
		Join<Student, OpenedCourse> joinOpenedCourse = fromStudent.join("courses");
		List<Predicate> conditions = new ArrayList<>();
		conditions.add(cb.equal(fromStudent.get("id"), studentId));
		TypedQuery<Student> tq = entityManager.createQuery(cq.select(fromStudent)
															 .where(conditions.toArray(new Predicate[]{})));
		List<Student> result = tq.getResultList();
		return result.size() > 0 ? result.get(0) : null;
	}
}
