package com.test.jpa.dao;

import java.util.List;

public interface EntityService<T, ID> {	
	void save(T entity);	
	void delete(T entity);
	T getById(ID id);
	List<T> getAll();
}
