package com.test.jpa.dao;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class EntityServiceImpl<T, ID> implements EntityService<T, ID>{

	private EntityManager entityManager;
	private final Class<T> clazz;
	
	public EntityServiceImpl(EntityManager entityManager, Class<T> clazz) {
		this.entityManager = entityManager;
		this.clazz = clazz;
	}
			
	@Override
	public void save(T entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(entity);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public void delete(T entity) {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entity);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
	}

	@Override
	public T getById(ID id) {
		try {
			entityManager.getTransaction().begin();
			T entity = entityManager.find(clazz, id);			
			entityManager.getTransaction().commit();			
			return entity;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		return null;
	}

	@Override
	public List<T> getAll() {
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(clazz);
			
			Root<T> from = cq.from(clazz);
			CriteriaQuery<T> select = cq.select(from);
			TypedQuery<T> tq = entityManager.createQuery(select);
			List<T> result = tq.getResultList();
			
			return result;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
		}
		
		return Collections.emptyList();
	}
	
}
