package thach.assignment.springboot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Duration;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@SpringBootTest(classes=Application.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("mvc")
public class RestAPITest {

	public static final String LOGIN_URL = "/auth/login";
	public static final String GET_CUSTOMERNAME_URL = "/api/customer/getName?userId=%s";
	public static final String GET_CUSTOMERDATE_URL = "/api/customer/getStartDate?userId=%s";
	public static final String GET_CUSTOMERINFO_URL = "/api/customer/getInfo?userId=%s";
	public static final String HEALTH_CHECK_URL = "/actuator/health";
	
	public static final int TEST_CUS_ID = 1;
	private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
	private static final String HEADER_KEY_AUTH = "Authorization";
	
	private static final String ADMIN_ACC = "admin";
	private static final String ADMIN_PWD = "123";
	private static final String USER_ACC = "user";
	private static final String USER_PWD = "123";

	@Autowired
	private WebApplicationContext wac;

	@Autowired(required = true)
	private FilterChainProxy springSecurityFilterChain;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
	}
	
	@Test
	public void testHealthCheck() throws Exception{
		mockMvc.perform(get(HEALTH_CHECK_URL)
				.accept(CONTENT_TYPE)).andExpect(status().isOk()).andExpect(content().contentType(CONTENT_TYPE))
				.andExpect(content().string("{\"status\":\"UP\"}"));
	}

	@Test
	public void testGetCustomerNameAPI() throws Exception{
		int userId = 1;
		MockHttpServletResponse  response = mockMvc.perform(get(String.format(GET_CUSTOMERNAME_URL, userId))
				.accept(CONTENT_TYPE)).andExpect(status().isOk())
				.andExpect(content().contentType(CONTENT_TYPE))				
				.andReturn().getResponse();
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		
		JsonArray jsonArray = new JsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("name", "customer1");
		jsonArray.add(jsonObject);
		
		assertThat(response.getContentAsString()).isEqualTo(jsonArray.toString());                
	}
	
	@Test
	public void testGetCustomerStartDateAPI() throws Exception{
		String token = getToken(USER_ACC, USER_PWD);
		assertThat(token);
		int userId = 1;
		MockHttpServletResponse  response = mockMvc.perform(get(String.format(GET_CUSTOMERDATE_URL, userId))
				.header(HEADER_KEY_AUTH, token)
				.accept(CONTENT_TYPE)).andExpect(status().isOk())
				.andExpect(content().contentType(CONTENT_TYPE))				
				.andReturn().getResponse();
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		
		JsonArray jsonArray = new JsonArray();
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("start_date", "2018-06-01 00:00:00");		
		jsonArray.add(jsonObject);
		
		assertThat(response.getContentAsString()).isEqualTo(jsonArray.toString());                
	}
	
	@Test
	public void testGetCustomerInfoAPI() throws Exception{
		String token = getToken(ADMIN_ACC, ADMIN_PWD);
		assertThat(token);
		int userId = 1;
		MockHttpServletResponse  response = mockMvc.perform(get(String.format(GET_CUSTOMERINFO_URL, userId))
				.header(HEADER_KEY_AUTH, token)
				.accept(CONTENT_TYPE)).andExpect(status().isOk())
				.andExpect(content().contentType(CONTENT_TYPE))				
				.andReturn().getResponse();
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
		
		JsonArray jsonArray = new JsonArray();
		JsonObject jsonObject = new JsonObject();		
        jsonObject.addProperty("id", 1);
		jsonObject.addProperty("name", "customer1");
		jsonObject.addProperty("start_date", "2018-06-01 00:00:00");
		jsonObject.addProperty("loyal_point", this.calcLoyal_point("2018-06-01 00:00:00"));		
		jsonArray.add(jsonObject);
		
		assertThat(response.getContentAsString()).isEqualTo(jsonArray.toString());                
	}

	private long calcLoyal_point(String userStartDate) {
		if(userStartDate != null) {
			userStartDate = userStartDate.substring(0, 10);
			LocalDate today = LocalDate.now();
			LocalDate startDate = LocalDate.parse(userStartDate);
			Duration diff = Duration.between(startDate.atStartOfDay(), today.atStartOfDay());
			return (diff.toDays() + 1)*5;
		}	
		return 0;
	}
	
	private String getToken(String username, String password) {
		try {			
			JsonObject credential = new JsonObject();
			credential.addProperty("username", username);
			credential.addProperty("password", password);
			String body = credential.toString();

			ResultActions result = mockMvc
					.perform(post(LOGIN_URL).contentType(MediaType.APPLICATION_JSON).content(body));
					
			String resultString = result.andReturn().getResponse().getContentAsString();

			JacksonJsonParser jsonParser = new JacksonJsonParser();
			String token = jsonParser.parseMap(resultString).get("token_value").toString();
			return token;

		} catch (Exception e) {
			return null;
		}

	}
}
