package thach.assignment.springboot.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import thach.assignment.springboot.security.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername( String username );   
}

