package thach.assignment.springboot.security.model;

public enum UserRoleName {
    ROLE_USER,
    ROLE_ADMIN
}
