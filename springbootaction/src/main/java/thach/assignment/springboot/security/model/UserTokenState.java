package thach.assignment.springboot.security.model;

public class UserTokenState {
	private String token_key;
	private String token_value;
	private Long expires_in;

	public UserTokenState() {
		this.token_key = null;
		this.token_value = null;
		this.expires_in = null;
	}

	public UserTokenState(String key, String value, long expires_in) {
		this.token_key = key;
		this.token_value = value;
		this.expires_in = expires_in;
	}

	public String getToken_key() {
		return token_key;
	}

	public void setToken_key(String token_key) {
		this.token_key = token_key;
	}

	public String getToken_value() {
		return token_value;
	}

	public void setToken_value(String token_value) {
		this.token_value = token_value;
	}

	public Long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}
}