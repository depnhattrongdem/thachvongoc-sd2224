package thach.assignment.springboot.security.model;

public interface UserService {
//    User findById(Long id);
    User findByUsername(String username);
//    List<User> findAll ();
}
