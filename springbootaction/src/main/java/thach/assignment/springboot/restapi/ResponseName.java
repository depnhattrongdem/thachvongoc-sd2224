package thach.assignment.springboot.restapi;

public class ResponseName {
	private String name;

	public ResponseName() {
		super();
	}
	
	public ResponseName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
