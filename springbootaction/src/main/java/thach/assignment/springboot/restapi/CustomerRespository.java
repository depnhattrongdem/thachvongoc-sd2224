package thach.assignment.springboot.restapi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRespository extends JpaRepository<Customer, Integer>{
	public List<Customer> findById(int userId);
}
