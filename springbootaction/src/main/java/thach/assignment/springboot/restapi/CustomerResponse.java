package thach.assignment.springboot.restapi;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Date;

public class CustomerResponse {
	private int id;
	private String name;
	private String start_date;
	private long loyal_point;
	
	public CustomerResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public CustomerResponse(int id, String name, String startDate, long loyalPoint) {
		this.id = id;
		this.name = name;
		this.start_date = startDate;
		this.loyal_point = loyalPoint;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public long getLoyal_point() {
		return loyal_point;
	}
	public void setLoyal_point(long loyal_point) {
		this.loyal_point = loyal_point;
	}	
	
	public void calcLoyal_point(Date userStartDate) {
		if(userStartDate != null) {
			LocalDate today = LocalDate.now();
			LocalDate startDate = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(userStartDate));
			Duration diff = Duration.between(startDate.atStartOfDay(), today.atStartOfDay());
			this.loyal_point = (diff.toDays() + 1)*5;
		}		
	}	
}
