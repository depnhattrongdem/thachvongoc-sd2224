package thach.assignment.springboot.restapi;

public class ResponseStartDate {
	private String start_date;

	public ResponseStartDate() {
		super();
	}
	
	public ResponseStartDate(String date) {
		this.start_date = date;
	}
	
	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	
}
