package thach.assignment.springboot.restapi;


import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value=CustomerController.PATH.ROOT)
public class CustomerController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	
	public static final class PATH{
		public static final String ROOT = "api/customer";
		public static final String URI_GETUSERNAME = "getName";
		public static final String URI_GETSTARTDATE = "getStartDate";
		public static final String URI_GETUSERINFO = "getInfo";
	}	
	
	@Autowired
	private CustomerRespository userService;

	private long calcLoyal_point(Date userStartDate) {
		if(userStartDate != null) {
			LocalDate today = LocalDate.now();
			LocalDate startDate = LocalDate.parse(new SimpleDateFormat("yyyy-MM-dd").format(userStartDate));
			Duration diff = Duration.between(startDate.atStartOfDay(), today.atStartOfDay());
			return (diff.toDays() + 1)*5;
		}	
		return 0;
	}	
	
	private String dateFormat(Date date) {
		if(date !=  null) {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		}
		return "";
	}
	
	@RequestMapping(method=RequestMethod.GET, value=CustomerController.PATH.URI_GETUSERNAME)
	public @ResponseBody List<ResponseName> getUserName(@RequestParam(value = "userId") int userId) {
		List<Customer> users = userService.findById(userId);
		List<ResponseName> name = users.stream().map(user -> new ResponseName(user.getName())).collect(Collectors.toList());

		return name;
	}

	@RequestMapping(method=RequestMethod.GET, value=CustomerController.PATH.URI_GETSTARTDATE)
	@PreAuthorize("hasRole('USER')")
	public @ResponseBody List<ResponseStartDate> getStartDate(@RequestParam(value = "userId") int userId) {
		List<Customer> users = userService.findById(userId);
		List<ResponseStartDate> startDate = users.stream().map(user -> new ResponseStartDate(dateFormat(user.getStart_date()))).collect(Collectors.toList());		

		return startDate;
	}

	@RequestMapping(method=RequestMethod.GET, value=CustomerController.PATH.URI_GETUSERINFO)
	@PreAuthorize("hasRole('ADMIN')")
	public @ResponseBody List<CustomerResponse> getUserInfo(@RequestParam(value = "userId") int userId) {
		List<Customer> resultSet = userService.findById(userId);
		List<CustomerResponse> dtos = resultSet.stream().map(record -> new CustomerResponse(record.getId(),
																					record.getName(),
																					this.dateFormat(record.getStart_date()),
																					this.calcLoyal_point(record.getStart_date()))).collect(Collectors.toList());
		return dtos;
	}
}
