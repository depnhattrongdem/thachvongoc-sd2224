1. Setup requirement
	- Link GIT:
	- Server: Tomcat
	- Server port: 8080
	- Authentication:
		- Admin account: admin/123
		- User account: user/123
	- Database: MySQL
	- Run import.sql file
2. APIs
	2.1 Login to get token
		- link: http://localhost:8080/auth/login
		- Method: POST
		- Request body in sample:
			{"username":"admin","password":"123"}
		- Response in sample:
			{
				"token_key": "Authorization",
				"token_value": "abcdxyz",
				"expires_in": 300
			}
	2.2 Get customer name by id
		- link: http://localhost:8080/api/customer/getName?userId=1
		- Method: GET
		- Header key: Authorization
		- Header value: string of token
		- Response in sample:
			[
				{
					"name": "customer3"
				}
			]
	2.3 Get customer start_date by id
		- link: http://localhost:8080/api/customer/getStartDate?userId=1
		- Method: GET
		- Header key: Authorization
		- Header value: string of token
		- Response in sample:
			[
				{
					"start_date": "2018-06-03 00:00:00"
				}
			]
	2.4 Get customer info by id
		- link: http://localhost:8080/api/customer/getInfo?userId=1
		- Method: GET
		- Header key: Authorization
		- Header value: string of token
		- Response in sample:
			[
				{
					"id": 1,
					"name": "customer1",
					"start_date": "2018-06-01 00:00:00",
					"loyal_point": 75
				}
			]
3. Unit test:
	@Test
	public void testHealthCheck()
	@Test
	public void testGetCustomerNameAPI()
	@Test
	public void testGetCustomerStartDateAPI()
	@Test
	public void testGetCustomerInfoAPI()