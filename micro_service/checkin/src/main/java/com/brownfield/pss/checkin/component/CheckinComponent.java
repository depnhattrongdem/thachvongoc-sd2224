package com.brownfield.pss.checkin.component;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.stereotype.Component;
import com.brownfield.pss.checkin.entity.CheckInRecord;
import com.brownfield.pss.checkin.entity.Luggage;
import com.brownfield.pss.checkin.repository.CheckinRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@EnableFeignClients
@RefreshScope
@Component
public class CheckinComponent {
	private static final Logger logger = LoggerFactory.getLogger(CheckinComponent.class);

	CheckinRepository checkinRepository;
	Sender sender;
	LuggageServiceProxy luggageService;
	
	@Autowired
	CheckinComponent(CheckinRepository checkinRepository, Sender sender, LuggageServiceProxy luggageService){
		this.checkinRepository = checkinRepository;
		this.sender = sender;
		this.luggageService = luggageService;
	}

	public long checkIn(CheckInRecord checkIn) {
		checkIn.setCheckInTime(new Date());
		logger.info("Saving checkin ");
		
		//save
		long id = checkinRepository.save(checkIn).getId();
		logger.info("Successfully saved checkin ");
		
		// fire saving luggage
		Luggage luggage = new Luggage(id, checkIn.getBookingId(), (checkIn.getFirstName() + " " + checkIn.getLastName()));
		try {			
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(luggage);
			long luggageId = luggageService.saveLuggage(json);
			logger.info("Successfully saved luggage with id: " + luggageId);
		} catch (JsonProcessingException e) {
			logger.info("Saving luggage fail");
			logger.error(e.getMessage());
		}		
		
		//send a message back to booking to update status
		logger.info("Sending booking id "+ id);
		sender.send(id);	
		return id;
		
	}
	
	public CheckInRecord getCheckInRecord(long id){
		return checkinRepository.findOne(id);
	}
	
}	
