package com.brownfield.pss.checkin.entity;

import java.util.Random;

public class Luggage {
	private long checkingId;
    private long bookingId;
    private int weight;
    private String passengerName;
    
    
    public Luggage() {
    	super();
	}
    
    public Luggage(long checkinId, long bookingId, String passengerName) {
    	this.checkingId = checkinId;
    	this.bookingId = bookingId;
    	int random = new Random().nextInt(20);
    	random = random == 0 ? 20 : random;
    	this.weight = random;
    	this.passengerName = passengerName;
	}
    
	public long getCheckingId() {
		return checkingId;
	}
	public void setCheckingId(long checkingId) {
		this.checkingId = checkingId;
	}
	public long getBookingId() {
		return bookingId;
	}
	public void setBookingId(long bookingId) {
		this.bookingId = bookingId;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
}
