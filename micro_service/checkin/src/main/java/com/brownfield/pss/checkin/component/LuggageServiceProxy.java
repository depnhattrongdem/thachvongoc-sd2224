package com.brownfield.pss.checkin.component;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="luggages-service")
public interface LuggageServiceProxy {

	@RequestMapping(value="luggages/luggage", method=RequestMethod.POST, headers = {"content-type=application/json"})
	long saveLuggage(@RequestBody String jsonLuggage);
}
