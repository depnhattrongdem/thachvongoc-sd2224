package com.brownfield.pss.luggage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brownfield.pss.luggage.component.LuggageComponent;
import com.brownfield.pss.luggage.entity.Luggage;

@RestController
@CrossOrigin
@RequestMapping("/luggages")
public class LuggageController {

	private static final Logger logger = LoggerFactory.getLogger(LuggageController.class);
	
	LuggageComponent luggageComponent;
	
	public LuggageController() {
	}
	
	@Autowired
	public LuggageController(LuggageComponent component) {
		this.luggageComponent = component;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/luggage/{checkingId}")	
	public Luggage getLuggage(@PathVariable(value="checkingId") long checkingId){
		return luggageComponent.getLuggage(checkingId);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/luggage")	
	public long saveLuggage(@RequestBody Luggage luggage){
		return luggageComponent.saveLuggage(luggage);
	}
}
