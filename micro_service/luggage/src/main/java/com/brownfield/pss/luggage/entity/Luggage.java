package com.brownfield.pss.luggage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Luggage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "luggages_luggage_generator")
	@SequenceGenerator(name = "luggages_luggage_generator", sequenceName = "seq_luggage", allocationSize=1)
	long id;
		
	private long checkingId;
    private long bookingId;
    private int weight;
    private String passengerName;
    
    public Luggage() {
	}
    
    public Luggage(long bookingId, int weight, String passengerName) {
    	this.bookingId = bookingId;
    	this.weight = weight;
    	this.passengerName = passengerName;
	}
    
    public Luggage(long bookingId, int weight) {
    	this.bookingId = bookingId;
    	this.weight = weight;
    	this.passengerName = "";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBookingId() {
		return bookingId;
	}

	public void setBookingId(long bookingId) {
		this.bookingId = bookingId;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getPassengerName() {
		return passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	
    
    public long getCheckingId() {
		return checkingId;
	}

	public void setCheckingId(long checkingId) {
		this.checkingId = checkingId;
	}

	public String toString(){
    	return "Luggage [id=" + id + ", weight=" + weight + ", passengerName=" + passengerName + ", bookingId" + bookingId + ", checkinId" + checkingId + "]";
    }
}
