package com.brownfield.pss.luggage.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.brownfield.pss.luggage.entity.Luggage;
import com.brownfield.pss.luggage.respository.LuggagesRespository;

@Component
public class LuggageComponent {
	
	private static final Logger logger = LoggerFactory.getLogger(LuggageComponent.class);
	LuggagesRespository luggageRespository;
	
	public LuggageComponent() {
	}
	
	@Autowired
	public LuggageComponent(LuggagesRespository respository) {
		this.luggageRespository = respository;
	}
	
	public Luggage getLuggage(long checkinId){
		logger.info("Looking for luggage by checkinId: "+ checkinId);
		Luggage result = luggageRespository.getLuggageByCheckingId(checkinId);
		logger.info("Found luggage: "+ result.toString());
		
		return result;
	}
	
	public long saveLuggage(Luggage luggage){
		logger.info("Saving a new luggage: " + luggage.toString());
		long id = luggageRespository.save(luggage).getId();
		logger.info("Saved  successfully with id: " + id);
		
		return id;
	}
}
