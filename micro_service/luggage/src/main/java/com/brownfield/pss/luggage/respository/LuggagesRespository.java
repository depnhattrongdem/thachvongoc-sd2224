package com.brownfield.pss.luggage.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brownfield.pss.luggage.entity.Luggage;

public interface LuggagesRespository extends JpaRepository<Luggage, Long>{
	Luggage getLuggageByCheckingId(long checkingId);
}
